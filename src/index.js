import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import Form from './components/form';
import 'bootstrap/dist/css/bootstrap.min.css';
import HomeScreen from './components/homeScreen';
import HomeS from './components/homeS';
import MainProducts from './components/mainProducts';
import MainProducts2 from './mainProducts2';


ReactDOM.render(
  <React.StrictMode>
   <Form/>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
