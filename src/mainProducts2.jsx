import React from "react";
import { Component } from "react/cjs/react.production.min";


class MainProducts2 extends Component
{
    state = {
        newCus : {basket:[],rad:""},
        products : [{ id: "PEP110", name: "Pepsi", category: "Food", stock: "yes" },
                    { id: "CLO876", name: "Close Up", category: "Toothpaste", stock: "no" },
                    { id: "PEA531", name: "Pears", category: "Soap", stock: "yes" },
                    { id: "LU7264", name: "Lux", category: "Soap", stock: "arriving" },
                    { id: "COL112", name: "Colgate", category: "Toothpaste", stock: "yes" },
                    { id: "DM881", name: "Dairy Milk", category: "Food", stock: "no" },
                    { id: "LI130", name: "Liril", category: "Soap", stock: "yes" },
                    { id: "PPS613", name: "Pepsodent", category: "Toothpaste", stock: "arriving" },
                    { id: "MAG441", name: "Maggi", category: "Food", stock: "yes" }],
        filteredProd:[{ id: "PEP110", name: "Pepsi", category: "Food", stock: "no" },
                    { id: "CLO876", name: "Close Up", category: "Toothpaste", stock: "arriving" },
                    { id: "PEA531", name: "Pears", category: "Soap", stock: "yes" },
                    { id: "LU7264", name: "Lux", category: "Soap", stock: "no" },
                    { id: "COL112", name: "Colgate", category: "Toothpaste", stock: "yes" },
                    { id: "DM881", name: "Dairy Milk", category: "Food", stock: "arriving" },
                    { id: "LI130", name: "Liril", category: "Soap", stock: "no" },
                    { id: "PPS613", name: "Pepsodent", category: "Toothpaste", stock: "no" },
                    { id: "MAG441", name: "Maggi", category: "Food", stock: "no" }],
        category : ["Soap","Food","Toothpaste"],
        stock : ["yes","no","arriving"]
           
    }

    handleChange = (e) =>
    {
        let json = {...this.state}
        let {currentTarget : input} = e

            input.type === "checkbox"
            ? (input.name === "category") 
            ? json = this.update(input.value,input.checked,json.newCus.basket) 
            : json.newCus[input.name] = input.checked
            : json.newCus[input.name] = input.value 

            this.setState(json);
        }
        
        update = (value,checked,arr) =>
        {

            let json = {...this.state}  
            if(checked) arr.push(value);
            else
            {
                let index = arr.findIndex( p => p===value);
                if(index >= 0) arr.splice(index,1) 
            }
            let temp = json.products.filter(item=>arr.includes(item.category));
            console.log(temp)
            json.filteredProd = temp;
            json.newCus.basket = arr;

        return json
    }


    edit = (i) =>
    {
        let json = {...this.state}
        let toFind = json.customers[i];
        json.newCus = toFind;
        json.view = 1;
        this.setState(json);
    }
    find = (s) =>
    {
        let f = this.state.newCus.basket.findIndex( (p) => p===s ) >=0
        return (f)
    }

    render()
    {
        let {category,filteredProd,stock,newCus} = {...this.state};    

        let f = filteredProd.filter( p => p.stock === newCus.rad);
        newCus.rad ? filteredProd = f : filteredProd = filteredProd
  

        return(<div className="container">

        <div className="row">

                <div className="col-2">
                <h4>Filter By Category</h4>

                    {category.map( s => (
                    <div className="form-check ">
                            <input 
                                type="checkbox"
                                className="form-check-input"
                                name="category"
                                value={s}
                                checked={this.find(s)}
                                onChange={this.handleChange}
                            />
                        <label className="form-check-label">{s}</label>
                    </div>
                    ))} 

                <h4>Filter By Stock</h4>

                {stock.map( p => (

                    <div className="form-check ">
                        <input 
                        type="radio"
                        className="form-check-input"
                        name="rad"
                        value={p}
                        checked={newCus.rad === p}
                        onChange={this.handleChange}
                        />
                        <label className="form-check-label">{p}</label>
                    </div>

))} 

                </div>

            <div className="col-10">

                <h1>Selected Products : </h1>
                <div className="row bg-warning ">
                    <div className="col-3">Name</div>
                    <div className="col-3">Category</div>
                    <div className="col-3">Id</div>
                    <div className="col-3">In Stock</div>
                </div>
                
            
                {filteredProd.map( p => 
                    <div className="row bg-light">
                    <div className="col-3">{p.name}</div>
                    <div className="col-3">{p.category}</div>
                    <div className="col-3">{p.id}</div>
                    <div className="col-3">{p.stock}</div>
                    </div>)}
                
                
        </div>

        </div>

        </div>)
    }
}

export default MainProducts2