import React from "react";
import { Component } from "react/cjs/react.production.min";
import CustomerForm from "./customerForm";


class HomeScreen extends Component
{
    state = {
        customers : [],
        view : -1,
        newCus : {name:"",gender:"",deliver:"",payment:[],slot:"",country:"",city:"",working:"",compName:"",desig:"",study:"",clgName:"",course:"",rad:""},
        dSlots : ["Credit Card","Debit Card","Net Banking"],
        payments :["2pm-6pm","Before 10am"],
        locs: [{country: "India",cities: ["New Delhi", "Mumbai", "Bangalore", "Chennai", "Pune"]},
                {country: "USA",cities: ["Los Angeles", "Chicago", "New York", "Seattle", "Washington DC"]},
                {country: "France", cities: ["Paris", "Nice", "Lyon", "Cannes"]},
                {country: "Japan", cities: ["Tokyo", "Kyoto"]},
                {country: "China", cities: ["Shanghai", "Beijing", "Shenzen"]
               }]
    }

    newCustomer = () =>
    {
        let json = {...this.state}
        json.newCus = {name:"",gender:"",deliver:"",payment:[],slot:"",country:"",city:"",working:"",compName:"",desig:"",study:"",clgName:"",course:"",rad:""};
        json.view = 1;
        this.setState(json);
    }

    showList = () =>
    {
        let json = {...this.state}
        json.view = 2;
        this.setState(json);
    }

    submit = () =>
    {
        let json = {...this.state}
        let n = json.newCus;
        json.customers.push(n);
        json.view = 2;
        console.log(json);
        this.setState(json);
    }

    edit = (i) =>
    {
        let json = {...this.state}
        let toFind = json.customers[i];
        json.newCus = toFind;
        json.view = 1;
        this.setState(json);
    }

    render()
    {
        let {customers,view,newCus} = {...this.state}
        let pay = newCus.payment.map( p => p);
        let p = pay.join(",");

        return(<div className="container">

        <button onClick={() =>this.newCustomer()} className="btn btn-warning">New Customer</button>
        <button onClick={() =>this.showList()} className="btn btn-warning">List Of Customers</button>

        {
            view===1 ? <CustomerForm state={this.state} onSubmit={this.submit}/>

             :

            view === 2 ?  
       
            customers.length === 0 ? <h2>There are no Customers</h2> : 
            
                <div> 
                <div className="row border bg-dark text-white">
                <div onClick={()=> this.sortName()} className="col-2 border ">Name</div>
                <div  onClick={()=> this.sortCategory()} className="col-2 border ">Gender</div>
                <div onClick={()=> this.sortSales()} className="col-2 border">Delivery</div>
                <div onClick={()=> this.sortSales()} className="col-2 border">Payments</div>
                <div onClick={()=> this.sortSales()} className="col-2 border">Slots</div>
                <div onClick={()=> this.sortSales()} className="col-2 border">Slots</div>
                </div>

                {customers.map( (c,index) =><div className="row border">
                    <div  className="col-2 border ">{c.name}</div>
                    <div  className="col-2 border ">{c.gender}</div>
                    <div  className="col-2 border">{c.deliver}</div>
                    <div  className="col-2 border">{p}</div>
                    <div  className="col-2 border">{c.slot}</div>
                    <div  className="col-2 border">  <button onClick={() =>this.edit(index)} className="btn btn-warning">Edit</button></div>
                    </div>
                )}
  
               </div>
            
             :

             <h1>view3</h1>
        }


        </div>)
    }
}

export default HomeScreen