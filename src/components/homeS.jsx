import React from "react";
import { Component } from "react/cjs/react.production.min";
import StudentForm from "./studentForm";
import MarksForm from "./marksForm";

class HomeS extends Component
{
    state = {
        customers : [{name:"Vishal",course:"Btech",year:"1999",marks:[{maths:"",english:"",comp:"",science:""}]}],
        view : -1,
        newCus : {name:"",course:"",year:"",marks:[{maths:0,english:0,comp:0,science:0}]}
    }

    newStudent = () =>
    {
        let json = {...this.state}
        json.newCus = {name:"",course:"",year:"",marks:[{maths:"",english:"",comp:"",science:""}]};
        json.view = 1;
        this.setState(json);
    }

    showList = () =>
    {
        let json = {...this.state}
        json.view = 2;
        this.setState(json);
    }

    showMarksForm = (i) =>
    {
        let json = {...this.state}
        json.newCus = {name:"",course:"",year:"",marks:[{maths:"",english:"",comp:"",science:""}]};
        let toFind = json.customers[i];
        json.newCus = toFind;
        json.view = 3; 
        this.setState(json);
    }

    submit = () =>
    {
        console.log("Hello");
        let json = {...this.state}
        let n = json.newCus;
        json.customers.push(n);
        json.view = 2;
        this.setState(json);
    }

    submitMarks = () =>
    {
        let json = {...this.state}
        let n = json.newCus;
        json.view = 2;
        this.setState(json);
    }

    edit = (i) =>
    {
        let json = {...this.state}
        let toFind = json.customers[i];
        json.newCus = toFind;
        json.view = 1;
        this.setState(json);
    }

    render()
    {
        let {customers,view} = {...this.state}
        console.log(customers);

        return(<div className="container">

        <button onClick={() =>this.newStudent()} className="btn btn-warning">New Student</button>
        <button onClick={() =>this.showList()} className="btn btn-warning">List Of Students</button>

        {
            view===1 ? <StudentForm state={this.state} onSubmit={this.submit}/>

             :

            view === 2 ?  
       
            customers.length === 0 ? <h2>There are no Students</h2> : 
            
                <div> 
                <div className="row border bg-dark text-white">
                <div onClick={()=> this.sortName()} className="col-2 border ">Name</div>
                <div  onClick={()=> this.sortCategory()} className="col-2 border ">Course</div>
                <div onClick={()=> this.sortSales()} className="col-2 border">Year</div>
                <div onClick={()=> this.sortSales()} className="col-2 border">Total Marks</div>
                <div onClick={()=> this.sortSales()} className="col-2 border">Result</div>
                </div>

                {customers.map( (c,index) =>
                    <div className="row border">
                    <div  className="col-2 border ">{c.name}</div>
                    <div  className="col-2 border ">{c.course}</div>
                    <div  className="col-2 border">{c.year}</div>
                    <div  className="col-2 border">{ c.marks[0].maths.length === 0 ? "No Data" : parseInt(c.marks[0].maths)+parseInt(c.marks[0].english)+parseInt(c.marks[0].comp)+parseInt(c.marks[0].science)}</div>
                    <div  className="col-2 border">{c.marks[0].maths.length === 0 ? <button onClick={() =>this.showMarksForm(index)} className="btn btn-warning">Enter</button> : <button onClick={() =>this.showMarksForm(index)} className="btn btn-warning">Edit</button>} </div>
                    
                    </div>
                )}
  
               </div>
            
             :

             view === 3 ?  

             <MarksForm state={this.state} onSubmit={this.submitMarks}/>

             :

             <h1></h1>
        }


        </div>)
    }
}

export default HomeS