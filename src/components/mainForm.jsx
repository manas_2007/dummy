
const { Component } = require("react");

class MainForm extends Component
{
    state =
    {
        courses : [],
        showCourse : false,
        showEdit : false,
        newCourse : {name:""},
        toEdit : ""
    }

    handleChange = (e) =>
    {
        let json = {...this.state};
        json.newCourse[e.currentTarget.name] = e.currentTarget.value;
        this.setState(json);   
    }
    submit = (e) =>
    {
        console.log("Hello");
        let json = {...this.state}
        e.preventDefault();
        let index = json.courses.findIndex( str => str === json.toEdit);
        index >= 1 ? json.courses[index] = json.newCourse.name :   json.courses.push(json.newCourse.name);
        json.showCourse =false;
        this.setState(json);
    }

    add = () =>
    {
        let json = {...this.state}
        json.showCourse =true;
        this.setState(json);
    }

    edit = (i) =>
    {
        let json = {...this.state}
        let toFind = json.courses[i];
        json.toEdit = toFind;
        json.newCourse.name = toFind;
        json.showCourse =true;
        this.setState(json);
    }

    render()
    {
        let {courses,showCourse,newCourse} = {...this.state}
        return(<div className="container">

        {showCourse === true ? <div className="container">
        <div className="form-group">
            <label>Add a Course</label>
            <input 
            type="text"
            className="form-control"
            id="name"
            name="name"
            value={newCourse.name}
            placeholder="Enter name"
            onChange={this.handleChange}
            />
            </div>
            <button onClick ={this.submit} className="btn btn-warning">Submit</button>
            </div>
           :  
          <button onClick ={this.add} className="btn btn-warning">Add a course</button>  
        }
            <h1>List Of Courses</h1>
            {   courses.length == 0 ? <p>There are zero courses</p> 
            :
                <ul>
                {courses.map( (n,index) => (<li>{n}  <button onClick={() =>this.edit(index)} className="btn btn-warning">Edit</button></li>))}
                </ul>
             }
        </div>
        )
    }
}
export default MainForm