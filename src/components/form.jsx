const { Component } = require("react");

class Form extends Component
{

    state = {
        courses : {name:"",age:"",city:"",adrs:""},
        errors : {}
    }

    submit = () =>
    {
       let json = {...this.state}
       this.setState(json);
    }

    handleError = (e) =>
    {
        let json = {...this.state}
        let {currentTarget : input} = e
        console.log(input);

        switch(input.name)
        {
            case ("name") : this.validateName()
            break;

            case ("age") : this.validateAge()
            break;

            case ("city") : this.validateCity()
            break
            default : break
        }
        
        this.setState(json);
    }

    validateName = () =>
    {
        let json = {...this.state}
        !json.courses.name 
        ? json.errors.name = "Name must be Entered"
        : json.courses.name.length <=5
        ? json.errors.name = "Enter minimun 5 characters"
        : json.errors.name = ""

    }

    validateAge = () =>
    {
        let json = {...this.state}
        !json.courses.age 
        ? json.errors.age = "Enter Age"
        : json.courses.age <= 20
        ? json.errors.age = "Age should be greater than 20"
        : json.errors.age = ""

    }

    validateCity = () =>
    {
        let json = {...this.state}
        !json.courses.city 
        ? json.errors.city = "Enter City"
        : json.courses.city.length <=3
        ? json.errors.city = "Enter minimun 3 characters"
        : json.errors.city = ""

    }

    handleChange = (e) =>
    {
        let json = {...this.state};
        json.courses[e.currentTarget.name] = e.currentTarget.value;
        this.setState(json);   
    }

    checkDisable = () =>
    {
        let json = {...this.state};
    
        if(!json.errors.name && !json.errors.age && !json.errors.city ) return false
        else return true

    }

    render()
    {
        let {name,age,city,adrs} = this.state.courses
        let {errors} = {...this.state}
        return( <div className="container">
        <h1>Enter details of Employee</h1>
        
        <div className="form-group">
            <label>Name </label>
            <input 
            type="text"
            className="form-control"
            id="name"
            name="name"
            value={name}
            placeholder="Enter name"
            onBlur = {this.handleError}
            onChange={this.handleChange}
            />
            <span className ="text-danger">{errors.name}</span>
        </div>

        <div className="form-group">
            <label>Age</label>
            <input 
            type="text"
            className="form-control"
            name="age"
            value={age}
            placeholder="Enter Course Description"
            onBlur = {this.handleError}
            onChange={this.handleChange}
            />
             <span className ="text-danger">{errors.age}</span>
        </div>

        <div className="form-group">
            <label>City</label>
            <input 
            type="text"
            className="form-control"
            name="city"
            value={city}
            placeholder="Enter course duration"
            onBlur = {this.handleError}
            onChange={this.handleChange}
            />
             <span className ="text-danger">{errors.city}</span>
        </div>

        <div className="form-group">
            <label>Address</label>
            <input 
            type="text"
            className="form-control"
            name="adrs"
            value={adrs}
            placeholder="Enter faculty name"
            onChange={this.handleChange}
            />
             <span className ="text-danger">{errors.adrs}</span>
        </div>

        <button onClick ={this.submit} disabled={this.checkDisable()} className="btn btn-warning">Submit</button>
        
        </div>
        
        
        
        )
    }
}

export default Form