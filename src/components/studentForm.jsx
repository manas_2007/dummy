import React from "react";
import { Component } from "react/cjs/react.production.min";


class StudentForm extends Component
{
    state = this.props.state

    handleChange = (e) =>
    {
        let json = {...this.state}
        json.newCus[e.currentTarget.name] = e.currentTarget.value;
        this.setState(json);
    }

    render()
    {
        let {newCus} = {...this.state}
        return( <React.Fragment>

            <div className="form-group">
            <label>Name</label>
            <input 
            type="text"
            className="form-control"
            id="name"
            name="name"
            value={newCus.name}
            placeholder="Enter Name"
            onChange={this.handleChange}
            />
            </div>

            <div className="form-group">
            <label>Course</label>
            <input 
            type="text"
            className="form-control"
            id="course"
            name="course"
            value={newCus.course}
            placeholder="Enter Course"
            onChange={this.handleChange}
            />
            </div>

            <div className="form-group">
            <label>Year</label>
            <input 
            type="text"
            className="form-control"
            id="year"
            name="year"
            value={newCus.year}
            placeholder="Enter Year"
            onChange={this.handleChange}
            />
            </div>

            <button onClick ={this.props.onSubmit} className="btn btn-warning">Submit</button>

        </React.Fragment>)

    }
}

export default StudentForm