import React from "react";
import { Component } from "react/cjs/react.production.min";


class CustomerForm extends Component
{
    state = this.props.state

    handleChange = (e) =>
    {
        let {currentTarget : input} = e
        let json = {...this.state}
            input.type === "checkbox"
            ? (input.name === "payment") 
            ? json.newCus.payment =   this.update(input.value,input.checked,json.newCus.payment) 
            : json.newCus[input.name] = input.checked
            : json.newCus[input.name] = input.value ;
            this.setState(json);
    }

    update = (value,checked,arr) =>
    {
        console.log(value,checked,arr);
        if(checked) arr.push(value);
        else
        {
            let index = arr.findIndex( p => p===value);
            if(index >= 0) arr.splice(index,1)
        }
        return arr
    }

    find = (s) =>
    {
        let f = this.state.newCus.payment.findIndex( (p) => p===s ) >=0
        return (f)
    }
    render()
    {
        let {newCus,dSlots,payments,locs} = {...this.state}
        const cities = newCus.country ? locs.find( c => c.country===newCus.country).cities : [];

        return( <React.Fragment>

            {/* Name */}

            <div className="form-group">
            <label>Name</label>
            <input 
            type="text"
            className="form-control"
            name="name"
            value={newCus.name}
            placeholder="Enter Name"
            onChange={this.handleChange}
            />
            </div>

            {/* Gender Radio */}

       
            <div className="form-check form-check-inline">
            <input 
            type="radio"
            className="form-check-input"
            name="gender"
            value="Male"
            checked={newCus.gender === "Male"}
            onChange={this.handleChange}
            />
            <label className="form-check-label">Male</label>
            </div>

            <div className="form-check form-check-inline">
            <input 
            type="radio"
            className="form-check-input"
            name="gender"
            value="Female"
            checked={newCus.gender === "Female"}
            onChange={this.handleChange}
            />
            <label className="form-check-label">Female</label>
            </div>

            <br />

            {/* Delivery Option Radio*/}

            <h4>Choose your Delivery Option</h4>

            <div className="form-check form-check-inline">
            <input 
            type="radio"
            className="form-check-input"
            name="deliver"
            value="Home"
            checked={newCus.deliver === "Home"}
            onChange={this.handleChange}
            />
            <label className="form-check-label">Home</label>
            </div>

            <div className="form-check form-check-inline">
            <input 
            type="radio"
            className="form-check-input"
            name="deliver"
            value="Office"
            checked={newCus.deliver === "Office"}
            onChange={this.handleChange}
            />
            <label className="form-check-label">Office</label>
            </div>

            <div className="form-check form-check-inline">
            <input 
            type="radio"
            className="form-check-input"
            name="deliver"
            value="Pickup"
            checked={newCus.deliver === "Pickup"}
            onChange={this.handleChange}
            />
            <label className="form-check-label">Pickup</label>
            </div>

            <br/>

            {/* Payment Checkbox */}

            
            <h4>Choose your Payment Option</h4>

           {dSlots.map( s => (

                <div className="form-check ">
                <input 
                type="checkbox"
                className="form-check-input"
                name="payment"
                value={s}
                checked={this.find(s)}
                onChange={this.handleChange}
                />
                <label className="form-check-label">{s}</label>
                </div>
            
           ))} 

            {/* Drop Down */}

            <div className="form-group">
            <label>Delivery Slot</label>
            <select
                className = "form-control"
                name="slot"
                value={newCus.slot}
                onChange={this.handleChange}
            >
            <option>
                Select the Delivery Slot
            </option>
            {
                payments.map( s => <option>{s}</option>)
            }
            </select>
            </div>


            {/* Country Drop Down */}


            <div className="form-group">
            <label>Country : </label>
            <select
                className = "form-control"
                name="country"
                value={newCus.country}
                onChange={this.handleChange}
            >
            <option>
                Select the Country
            </option>
            {
                locs.map( s => <option>{s.country}</option>)
            }
            </select>
            </div>


            {/* Cities Drop Down */}

            { newCus.country ? 
                <div className="form-group">
            <label>City</label>
            <select
                className = "form-control"
                name="city"
                value={newCus.city}
                onChange={this.handleChange}
            >
            <option>
                Select the City
            </option>
            {
                cities.map( c => <option>{c}</option>)
            }
            </select>
            </div>
            : 
            ""}

            <br />

            {/* Working CheckBox

            <div className="form-check ">
                <input 
                type="checkbox"
                className="form-check-input"
                name="working"
                value={newCus.working}
                checked={newCus.working}
                onChange={this.handleChange}
                />
                <label className="form-check-label">Working</label>
            </div>

            {newCus.working ? <div>
                <div className="form-group">
                <label>Company Name</label>
                <input 
                type="text"
                className="form-control"
                name="compName"
                value={newCus.compName}
                placeholder="Enter Company Name"
                onChange={this.handleChange}
            />
            </div>
            
            <div className="form-group">
                <label>Designation</label>
                <input 
                type="text"
                className="form-control"
                name="desig"
                value={newCus.desig}
                placeholder="Enter Designation"
                onChange={this.handleChange}
                />
            </div></div>
            : ""}

            {/* STUDYING CheckBox */}
{/* 
            <div className="form-check ">
                <input 
                type="checkbox"
                className="form-check-input"
                name="study"
                value={newCus.study}
                checked={newCus.study}
                onChange={this.handleChange}
                />
                <label className="form-check-label">Studying</label>
            </div>

            {newCus.study ? <div>
                <div className="form-group">
                <label>College Name</label>
                <input 
                type="text"
                className="form-control"
                name="clgName"
                value={newCus.clgName}
                placeholder="Enter Company Name"
                onChange={this.handleChange}
            />
            </div>
            
            <div className="form-group">
                <label>Course</label>
                <input 
                type="text"
                className="form-control"
                name="course"
                value={newCus.course}
                placeholder="Enter Course"
                onChange={this.handleChange}
            />
            </div></div>
            : ""} */} 

             {/* Working Radio */}

             <div className="form-check ">
                <input 
                type="radio"
                className="form-check-input"
                name="rad"
                value="working"
                checked={newCus.rad === "working"}
                onChange={this.handleChange}
                />
                <label className="form-check-label">Working</label>
            </div>

            {newCus.rad === "working" ? <div>
                <div className="form-group">
                <label>Company Name</label>
                <input 
                type="text"
                className="form-control"
                name="compName"
                value={newCus.compName}
                placeholder="Enter Company Name"
                onChange={this.handleChange}
            />
            </div>
            
            <div className="form-group">
                <label>Designation</label>
                <input 
                type="text"
                className="form-control"
                name="desig"
                value={newCus.desig}
                placeholder="Enter Designation"
                onChange={this.handleChange}
                />
            </div></div>
            : ""}

            {/* STUDYING Radio */}

            <div className="form-check ">
                <input 
                type="radio"
                className="form-check-input"
                name="rad"
                value="study"
                checked={newCus.rad === "study"}
                onChange={this.handleChange}
                />
                <label className="form-check-label">Studying</label>
            </div>

            {newCus.rad === "study" ? <div>
                <div className="form-group">
                <label>College Name</label>
                <input 
                type="text"
                className="form-control"
                name="clgName"
                value={newCus.clgName}
                placeholder="Enter Company Name"
                onChange={this.handleChange}
            />
            </div>
            
            <div className="form-group">
                <label>Course</label>
                <input 
                type="text"
                className="form-control"
                name="course"
                value={newCus.course}
                placeholder="Enter Course"
                onChange={this.handleChange}
            />
            </div></div>
            : ""}

            

            <button onClick ={this.props.onSubmit} className="btn btn-warning">Submit</button>

        </React.Fragment>)

    }
}

export default CustomerForm