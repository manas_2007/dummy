import React, { useLayoutEffect } from "react";
import { Component } from "react/cjs/react.production.min";


class MainProducts extends Component
{
    state = {
        cart : [],
        newCus : {basket:[],rad:""},
        products : [{ id: "PEP110", name: "Pepsi", category: "Food", stock: "true" },
                    { id: "CLO876", name: "Close Up", category: "Toothpaste", stock: "false" },
                    { id: "PEA531", name: "Pears", category: "Soap", stock: "true" },
                    { id: "LU7264", name: "Lux", category: "Soap", stock: "false" },
                    { id: "COL112", name: "Colgate", category: "Toothpaste", stock: "true" },
                    { id: "DM881", name: "Dairy Milk", category: "Food", stock: "false" },
                    { id: "LI130", name: "Liril", category: "Soap", stock: "true" },
                    { id: "PPS613", name: "Pepsodent", category: "Toothpaste", stock: "false" },
                    { id: "MAG441", name: "Maggi", category: "Food", stock: "true" }],

        filteredProd:[{ id: "PEP110", name: "Pepsi", category: "Food", stock: "true" },
                    { id: "CLO876", name: "Close Up", category: "Toothpaste", stock: "false" },
                    { id: "PEA531", name: "Pears", category: "Soap", stock: "true" },
                    { id: "LU7264", name: "Lux", category: "Soap", stock: "false" },
                    { id: "COL112", name: "Colgate", category: "Toothpaste", stock: "true" },
                    { id: "DM881", name: "Dairy Milk", category: "Food", stock: "false" },
                    { id: "LI130", name: "Liril", category: "Soap", stock: "true" },
                    { id: "PPS613", name: "Pepsodent", category: "Toothpaste", stock: "false" },
                    { id: "MAG441", name: "Maggi", category: "Food", stock: "true" }],

        category : ["Soap","Food","Toothpaste"]
           
    }

    handleChange = (e) =>
    {
        let {currentTarget : input} = e
        let json = {...this.state}
            input.type === "checkbox"
            ? (input.name === "basket") 
            ? json.newCus.basket =   this.update(input.value,input.checked,json.newCus.basket) 
            : json.newCus[input.name] = input.checked
            : json.newCus[input.name] = input.value ;

            let f = json.products.filter( p => p.category ===  json.newCus.rad);
            
            json.filteredProd = f;
            this.setState(json);
    }

    update = (value,checked,arr) =>
    {
        console.log(value,checked,arr);
        let json = {...this.state}

        if(checked) arr.push(value);
        else
        {
            let index = arr.findIndex( p => p===value);
            let index2 = json.cart.findIndex( p => p.name===value);
            if(index >= 0) arr.splice(index,1)
            if(index2 >= 0) json.cart.splice(index,1)
        }
        return arr
    }


    edit = (i) =>
    {
        let json = {...this.state}
        let toFind = json.customers[i];
        json.newCus = toFind;
        json.view = 1;
        this.setState(json);
    }
    find = (s) =>
    {
        let f = this.state.newCus.basket.findIndex( (p) => p===s ) >=0
        return (f)
    }

    render()
    {
        let {products,newCus,category,filteredProd} = {...this.state}
    

        return(<div className="container">
        <div className="row">
        <div className="col-2">
       
        {category.map( p => (

            <div className="form-check ">
                <input 
                type="radio"
                className="form-check-input"
                name="rad"
                value={p}
                checked={newCus.rad === p}
                onChange={this.handleChange}
                />
                <label className="form-check-label">{p}</label>
            </div>

        ))} 
        </div>
        <div className="col-10">

        <h1>Selected Products : </h1>
                <div className="row bg-warning ">
                    <div className="col-3">Name</div>
                    <div className="col-3">Category</div>
                    <div className="col-3">Id</div>
                    <div className="col-3">In Stock</div>
                </div>

   
            {filteredProd.map( p => 
                <div className="row bg-light">
                <div className="col-3">{p.name}</div>
                <div className="col-3">{p.category}</div>
                <div className="col-3">{p.id}</div>
                <div className="col-3">{p.stock}</div>
                </div>)}
        </div>

        </div>

        </div>)
    }
}

export default MainProducts