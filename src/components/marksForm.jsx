import React from "react";
import { Component } from "react/cjs/react.production.min";


class MarksForm extends Component
{
    state = this.props.state

    handleChange = (e) =>
    {
        let json = {...this.state}
        let j = json.newCus.marks[0];
        j[e.currentTarget.name] = e.currentTarget.value;
        this.setState(json);
    }

    render()
    {
        let {newCus} = {...this.state}
        return( <React.Fragment>

            <h1>Enter marks for {newCus.name}</h1>

            <div className="form-group">
            <label>Maths</label>
            <input 
            type="text"
            className="form-control"
            id="maths"
            name="maths"
            value={newCus.marks[0].maths}
            placeholder="Enter Marks"
            onChange={this.handleChange}
            />
            </div>

            <div className="form-group">
            <label>English</label>
            <input 
            type="text"
            className="form-control"
            id="course"
            name="english"
            value={newCus.marks[0].english}
            placeholder="Enter Marks"
            onChange={this.handleChange}
            />
            </div>

            <div className="form-group">
            <label>Computers</label>
            <input 
            type="text"
            className="form-control"
            id="comp"
            name="comp"
            value={newCus.marks[0].comp}
            placeholder="Enter Marks"
            onChange={this.handleChange}
            />
            </div>

            <div className="form-group">
            <label>Science</label>
            <input 
            type="text"
            className="form-control"
            id="science"
            name="science"
            value={newCus.marks[0].science}
            placeholder="Enter Marks"
            onChange={this.handleChange}
            />
            </div>

            <button onClick ={this.props.onSubmit} className="btn btn-warning">Submit Marks</button>

        </React.Fragment>)

    }
}

export default MarksForm